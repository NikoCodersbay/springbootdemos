package com.keys.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Bestellung")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int orderNumber;

    @JsonIgnore
    @OneToMany(mappedBy = "order")
    List<Basket> basketList = new ArrayList<>();

    private String timestamp;

    public Order() {
    }

    public Order(String timestamp) {
        this.timestamp = timestamp;
    }

    public Order(int orderNumber, String timestamp) {
        this.orderNumber = orderNumber;
        this.timestamp = timestamp;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public List<Basket> getBasketList() {
        return basketList;
    }

    public void setBasketList(List<Basket> basketList) {
        this.basketList = basketList;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
