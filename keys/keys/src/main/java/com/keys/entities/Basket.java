package com.keys.entities;

import com.keys.keys.BasketPK;
import jakarta.persistence.*;
import lombok.*;
import org.aspectj.weaver.ast.Or;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@IdClass(BasketPK.class)
public class Basket {

    @Id
    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;

    @Id
    @ManyToOne
    @JoinColumn(name = "productId")
    private Product product;

    @Id
    @ManyToOne
    @JoinColumn(name = "orderNumber")
    private Order order;

    private int qty;

    public Basket(User user, Product product, int qty) {
        this.user = user;
        this.product = product;
        this.qty = qty;
    }
}
