package com.keys.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.keys.Role;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Benutzer")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userId;

    private String username;


    private Role role;

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    List<Basket> basketList = new ArrayList<>();

    public User(String username) {
        this.username = username;
    }
}
