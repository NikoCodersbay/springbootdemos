package com.keys.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int productId;

    private String productName;

    private String description;

    @JsonIgnore
    @OneToMany(mappedBy = "product")
    List<Basket> basketList = new ArrayList<>();

    public Product(String productName) {
        this.productName = productName;
    }
}
