package com.keys.repositories;

import com.keys.entities.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderCrudRepository extends CrudRepository<Order, Integer> {
}
