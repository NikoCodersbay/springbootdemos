package com.keys.repositories;

import com.keys.entities.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductCrudRepository extends CrudRepository<Product, Integer> {

    Product findByProductNameOrDescription(String productName, String description);


}
