package com.keys.repositories;

import com.keys.entities.Basket;
import com.keys.keys.BasketPK;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BasketCrudRepository extends CrudRepository<Basket, BasketPK> {
}
