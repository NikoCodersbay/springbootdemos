package com.keys.contoller;

import com.keys.DTOs.BasketDTO;
import com.keys.entities.Basket;
import com.keys.entities.Order;
import com.keys.entities.Product;
import com.keys.entities.User;
import com.keys.keys.BasketPK;
import com.keys.repositories.BasketCrudRepository;
import com.keys.repositories.OrderCrudRepository;
import com.keys.repositories.ProductCrudRepository;
import com.keys.repositories.UserCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("api")
public class TestContoller {

    @Autowired
    UserCrudRepository userCrudRepository;

    @Autowired
    ProductCrudRepository productCrudRepository;

    @Autowired
    BasketCrudRepository basketCrudRepository;

    @Autowired
    OrderCrudRepository orderCrudRepository;

    @PostMapping
    private String test(){

        User user = new User("Testuser");

        userCrudRepository.save(user);

        Product product = new Product("Testprodukt");

        Product product2 = new Product("Testprodukt2");


        productCrudRepository.save(product);

        productCrudRepository.save(product2);

        Order order = new Order("Timestamp");

        orderCrudRepository.save(order);

        Basket basket = new Basket(user, product, order, 10);

        basketCrudRepository.save(basket);

        Optional<Product> productOptional = productCrudRepository.findById(1);

        Product product1 = productOptional.get();

        Basket basket2 = new Basket(user, product2, order, 5);

        basketCrudRepository.save(basket2);

        return "Hi";
    }

    @GetMapping
    private BasketDTO test2(){

//        User user = userCrudRepository.findById(1).get();
//
//        Product product = productCrudRepository.findById(1).get();
//
//        Basket basket = basketCrudRepository.findById(new BasketPK(user,product, 1)).get();
//
//        BasketDTO basketDTO = new BasketDTO(basket.getUser(), basket.getProduct(), basket.getQty());

        return new BasketDTO();
    }
}
