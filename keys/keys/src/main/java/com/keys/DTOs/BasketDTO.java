package com.keys.DTOs;

import com.keys.entities.Product;
import com.keys.entities.User;
import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BasketDTO {

    private User user;

    private Product product;

    private int qty;
}
