package com.keys.keys;

import com.keys.entities.Order;
import com.keys.entities.Product;
import com.keys.entities.User;
import lombok.*;

import java.io.Serializable;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BasketPK implements Serializable {

    private User user;

    private Product product;

    private Order order;
}
